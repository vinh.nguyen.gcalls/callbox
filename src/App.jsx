import * as React from 'react'
import './App.css'
import { DialPad, Phoning } from './components/'
import { UserContext } from './context/'



const arrayNumberPhone = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '0', '#'];

function App() {
  const { isSearch } = React.useContext(UserContext);
  const [user, setUser] = isSearch;
  // console.log('user', user)

  const [phoneNumber, setPhoneNumber] = React.useState([]);
  const [isCalling, setIsCalling] = React.useState(false);
  const [isMute, setIsMute] = React.useState(false);
  const [isCallConnected, setIsCallConnected] = React.useState(false);

  return (
    <div className="app flex justify-center items-center">
      {isCalling ?
        <Phoning
          phoneNumber={phoneNumber}
          isCalling={isCalling}
          setIsCalling={setIsCalling}
          isMute={isMute}
          setIsMute={setIsMute}
          isCallConnected={isCallConnected}
          setIsCallConnected={setIsCallConnected}
        />
        :
        <DialPad
          data={arrayNumberPhone}
          phoneNumber={phoneNumber}
          setPhoneNumber={setPhoneNumber}
          isCalling={isCalling}
          setIsCalling={setIsCalling}
          isCallConnected={isCallConnected}
          setIsCallConnected={setIsCallConnected}
        />
      }
    </div>
  )
}

export default App
