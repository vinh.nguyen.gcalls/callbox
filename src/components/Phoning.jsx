import React, { useState, useEffect } from 'react';
import { StopCall, ToggleMute } from './index';

export function Phoning({ phoneNumber, isCalling, setIsCalling, isMute, setIsMute, isCallConnected, setIsCallConnected }) {

    const [timer, setTimer] = useState(0);

    useEffect(() => {
        let intervalId;

        if (isCallConnected) {
            // Bắt đầu đếm thời gian nếu isCallConnected là true
            intervalId = setInterval(() => {
                setTimer((prevTimer) => prevTimer + 1);
            }, 1000);
        }

        return () => {
            // Xóa interval khi component unmount hoặc isCallConnected thay đổi
            clearInterval(intervalId);
        };
    }, [isCallConnected]);

    const formatTime = (time) => {
        const hours = Math.floor(time / 3600);
        const minutes = Math.floor((time % 3600) / 60);
        const seconds = time % 60;

        return `${hours.toString().padStart(2, '0')}:${minutes
            .toString()
            .padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
    };


    return (
        <div>
            <div className='flex flex-col justify-center text-white'>
                <p className='text-3xl bg-transparent text-center block'>{phoneNumber}</p>
                <span>Gcalls</span>
                {isCallConnected ?
                    <span className='pt-10'>{formatTime(timer)}</span>

                    :
                    <span className='pt-10'>Đang gọi...</span>
                }
            </div>

            <div className='flex justify-center flex-wrap text-white py-40'>
                <div
                    style={{ borderColor: isMute ? 'red' : 'white', color: isMute ? 'red' : 'white' }}
                    className='flex border-solid border-2 rounded-full text-xl justify-center items-center w-16 h-16 mx-2 cursor-pointer'
                    onClick={() => ToggleMute(setIsMute, isMute)}
                >
                    <i className="fa-solid fa-microphone-slash"></i>
                </div>
                <div className='flex border-solid border-2 rounded-full text-xl justify-center items-center w-16 h-16 mx-2 cursor-pointer'>
                    <i className="fa-regular fa-keyboard"></i>
                </div>
                <div className='flex border-solid border-2 rounded-full text-xl justify-center items-center w-16 h-16 mx-2 cursor-pointer'>
                    <i className="fa-solid fa-pause"></i>
                </div>
                <div className='flex border-solid border-2 rounded-full text-xl justify-center items-center w-16 h-16 mx-2 cursor-pointer'>
                    <i className="fa-solid fa-phone-volume"></i>
                </div>
            </div>

            <button
                className="bg-red-500 hover:bg-red-700 text-white text-2xl font-bold py-2 px-8 rounded-3xl shadow-2xl mt-8"
                onClick={() => {
                    StopCall();
                    setIsCalling(!isCalling);
                }}
            >
                <i className="fa-solid fa-phone-slash"></i>
            </button>
        </div>
    )
}