import * as React from "react";

const UserContext = React.createContext(null);

const UseContextProvider = ({ children }) => {
    const [user, setUser] = React.useState(1);

    const state = {
        isSearch: [user, setUser]
    };

    return (
        <UserContext.Provider value={state} >
            {children}
        </UserContext.Provider>
    )
}


export { UserContext, UseContextProvider }
